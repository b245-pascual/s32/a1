// S32 node.js

const http = require('http');

	let port = 4000;

	http.createServer(function(req, res){
		//homepage
		if(req.url === "/" && req.method === "GET"){
				res.writeHead(200, {'Content-Type': 'text/plain'});
				res.write("Welcome to the Booking System!");
				res.end();
			}
		//profile	
			else if(req.url === "/profile" && req.method === "GET"){
				res.writeHead(200, {'Content-Type': 'text/plain'});
				res.write("Welcome to your profile!");
				res.end();
			}
		//course	
			else if(req.url === "/course" && req.method === "GET"){
				res.writeHead(200, {'Content-Type': 'text/plain'});
				res.write("Here are our available courses.");
				res.end();
			}
			else if(req.url === "/addCourse" && req.method === "POST"){
				res.writeHead(200, {'Content-Type': 'text/plain'});
				res.write("Add a course to our resources.");
				res.end();
			}
		//addcourse
			else if(req.url === "/updateCourse" && req.method === "PUT"){
				res.writeHead(200, {'Content-Type': 'text/plain'});
				res.write("Update a course in our resources.");
				res.end();
			}
		//delete course	
			else if(req.url === "/archiveCourse" && req.method === "DELETE"){
				res.writeHead(200, {'Content-Type': 'text/plain'});
				res.write("Archive a course in our resources.");
				res.end();
			} else {
				res.writeHead(404, {'Content-Type': 'text/plain'});
				res.write("Page Unavailable");
				res.end();
			}
			

	}).listen(port);

	console.log(`Server is running at port ${port}!`);



